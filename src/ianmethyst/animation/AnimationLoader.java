package ianmethyst.animation;

import java.io.File;
import processing.core.*;
import java.util.Collections;
import java.util.Arrays;
import java.util.List;


/**
 * Clase genérica para cargar animaciones. Debería ser estática, pero no se puede referenciar de manera
 * estática los métodos de PApplet, así que se debe crear un objeto de esta clase para ser usada como
 * una inner class.
 */
public class AnimationLoader {
	private PApplet p;

	public AnimationLoader(PApplet processing) {
		p = processing;
	}
	/**
	 * Este método carga una secuencia de imagenes de un directorio ordenandolas de manera alfabética
	 * @param path Directorio del cual cargar la secuencia de imagenes (absoluto)
	 * @param debug Determina si se debe imprimir el directorio de cada archivo
	 * @return Secuencia de imagenes. Usar con clase Animation.
	 */
	public PImage[] loadAnimation(String path, boolean debug) {
		File folder = new File(path);
		List<String> files = Arrays.asList(folder.list());

		//Collections.sort(files, Collections.reverseOrder());
		Collections.sort(files);

		PImage[] frames = new PImage[files.size()];

		for (int i = 0; i < frames.length; i++) {
			if (debug) {
				System.out.println(folder.getAbsolutePath() + "/" + files.get(i));
			}

			frames[i] = p.loadImage(folder.getAbsolutePath() + "/" + files.get(i));
		}

		return frames;
	}

	public PImage[] loadAnimation(String path) {
		return loadAnimation(path, false);
	}

	public String[] getFramePaths(String path) {
		File folder = new File(path);
		List<String> files = Arrays.asList(folder.list());

		Collections.sort(files);

		String[] paths = new String[files.size()];

		for (int i = 0; i < paths.length; i++) {
			paths[i] = folder.getAbsolutePath() + "/" + files.get(i);
		}

		return paths;
	}
}
