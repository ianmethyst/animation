package ianmethyst.animation;

import processing.core.*;

/**
 * Clase para animaciones (secuencia de imágenes) cargadas en un hilo.
 */

public class AsyncAnimation extends Animation {
  private String[] framePaths;
  private PImage currentImage;
  private PImage nextImage;

  private PApplet p;

  public AsyncAnimation(PApplet processing, String[] paths) {
    super();

    p = processing;
    framePaths = paths;

    currentImage = p.loadImage(framePaths[0]);
  }

  @Override
  public void update() {
    updateAlpha();

    if (nextImage == null) {
      if (currentFrame == framePaths.length - 1) {
        if (shouldLoop) {
        currentFrame = loopFrame;
        loopCount++;
        nextImage = p.requestImage(framePaths[loopFrame]);
        } else {
          remove = true;
          return;
        }
      } else {
        nextImage = p.requestImage(framePaths[currentFrame]);
      }
    }

    if (nextImage.width == -1) {
      System.out.println("ERROR: Image " + framePaths[currentFrame + 1] + "couldn't be loaded");
    }

    if (nextImage.width > 0) {
      currentImage = nextImage;
      nextImage = null;
      currentFrame++;
    }

    // Conditions for removal
    if (!infiniteLoop) {
      if (shouldLoop) {
        // Remove if loopCount reached loopTimes and alpha hit 0
        if (loopCount > loopTimes) {
          disappearing = true;
        }
      } else {
        // Remove if finished and loop is not set
        if ((!reverse && currentFrame == framePaths.length - 1) || (reverse && currentFrame == -1)) {
          remove = true;
        } 
      }
    }

    if (disappearing && alpha == 0) {
      remove = true;
    }
  }

  @Override
  public void render(PGraphics offscreen, float x, float y, float w, float h) {
    offscreen.tint(360, alpha);
    offscreen.image(currentImage, x, y, w, h);
  }

  @Override 
  public void jumpTo(int frame) {
    if (frame > framePaths.length) {
      currentFrame = framePaths.length - 1;
    } else if (frame < 0) {
      currentFrame = 0;
    } else {
      currentFrame = frame;
    }
  }
}
