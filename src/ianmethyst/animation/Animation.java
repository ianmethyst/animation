package ianmethyst.animation;

import processing.core.*;

/**
 * Clase para animaciones (secuencia de imágenes).
 */

public class Animation {
  private PImage[] frames;

  protected boolean playing;
  protected boolean reverse;
  protected int currentFrame;

  protected boolean disappearing;
  protected int alpha;

  protected int loopCount;
  protected int loopTimes;
  protected int loopFrame;
  protected boolean shouldLoop;
  protected boolean infiniteLoop;

  protected boolean remove;


  public Animation() {
    playing = false;
    reverse = false;
    currentFrame = 0;
    alpha = 255;

    shouldLoop = false;
    loopFrame = 0;
    loopTimes = 0;
    infiniteLoop = false;

    remove = false;
  }

  public Animation(PImage[] animation) {
    this();
    frames = animation;
  }

  public void update() {
    if (playing) {
      if (!reverse && currentFrame < frames.length - 1) {
        currentFrame++;

        if (shouldLoop && currentFrame == frames.length - 1) {
          currentFrame = loopFrame;
          loopCount++;
        }

      } else if (reverse && currentFrame > 0) {
        currentFrame--;

        if (shouldLoop && currentFrame == loopFrame ) {
          currentFrame = frames.length -1;
          loopCount++;
        }
      }
    }

    updateAlpha();

    // Conditions for removal
    if (!infiniteLoop) {
      if (shouldLoop) {
        // Remove if loopCount reached loopTimes and alpha hit 0
        if (loopCount > loopTimes) {
          disappearing = true;
        }
      } else {
        // Remove if finished and loop is not set
        if ((!reverse && currentFrame == frames.length - 1) || (reverse && currentFrame == 0)) {
          remove = true;
        } 
      }
    }

    if (disappearing && alpha == 0) {
      remove = true;
    }
  }

  protected void updateAlpha() {
    if (!disappearing  && alpha < 255) {
      alpha += 5;
    } else if (disappearing && alpha > 0) {
      alpha -= 5;
    }
  }

  public void render(PGraphics offscreen, float x, float y, float w, float h) {
    offscreen.tint(360, alpha);
    offscreen.image(frames[currentFrame], x, y, w, h);
  }

  public void render(PGraphics offscreen, float x, float y) {
    render(offscreen, x, y, frames[currentFrame].width, frames[currentFrame].height);
  }


  public void render(PGraphics offscreen) {
    render(offscreen, offscreen.width / 2, offscreen.height / 2, frames[currentFrame].width, frames[currentFrame].height);
  }


  public void setLoop (int times, int frame) {
    shouldLoop = true;
    infiniteLoop = false;
    loopTimes = times;
    loopFrame = frame;
  }

  public void setLoop (int times) {
    if (times == 0) {
      shouldLoop = false;
      return;
    }
    setLoop(times, 0);
  }

  public void setInfiniteLoop(int frame) {
    shouldLoop = true;
    infiniteLoop = true;
    loopTimes = 0;
    loopFrame = frame;
  }

  public void setInfiniteLoop() {
    setInfiniteLoop(0);
  }

  public void disableLoop() {
    shouldLoop = false;
    infiniteLoop = false;
    loopTimes = 0;
    loopFrame = 0;
  }

  public void setRemove(boolean b) {
    remove = b;
  }

  public void setAlpha(int a) {
    alpha = a;
  }

  public void setDisappear(boolean d) {
    disappearing = d;
  }

  public void play() {
    playing = true;
  }

  public void pause() {
    playing = false;
  }

  public void stop() {
    playing = false;
    reverse = false;
    currentFrame = 0;
  }

  public void jumpTo(int frame) {
    if (frame  > frames.length) {
      currentFrame = frames.length - 1;
    } else if (frame < 0) {
      currentFrame = 0;
    } else {
      currentFrame = frame;
    }
  }

  public int getAlpha() {
    return alpha;
  }

  public boolean isDisappearing() {
    return disappearing;
  }

  public boolean shouldRemove() {
    return remove;
  }

  public int getLoopCount() {
    return loopCount;
  }

  public int getLoopTimes() {
    return loopTimes;
  }

  public int getCurrentFrameIndex() {
    return currentFrame;
  }
}
